package com.bradar.android;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.IBinder;

public class Compass extends Service implements SensorEventListener{

	private SensorManager sensorManager;
	private Sensor sensor;
	private int degree;
	
	@SuppressWarnings("deprecation")
	public Compass(Context context)
	{
		sensorManager = (SensorManager)context.getSystemService(SENSOR_SERVICE);
		sensor = sensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION);
	}

	@Override
	public void onAccuracyChanged(Sensor arg0, int arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		// TODO Auto-generated method stub
		degree = Math.round(event.values[0]);
	}

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public void startListener()
	{
		sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_NORMAL);
	}
	
	public int getDegree()
	{
		return degree;
	}
	
	public void stopListener()
	{
		if (sensorManager != null)
		{
			sensorManager.unregisterListener(this);
		}
	}
}
