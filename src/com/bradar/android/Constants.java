package com.bradar.android;

import android.os.Environment;

public class Constants {
	
	public static final String group_id = "35825a69a28afe433c84e48c0cc2fd27";
	public static final String web_uri = "http://milestone.if.itb.ac.id/pbd/index.php";
	public static final String image_dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).getPath() + "/B.Radar/";
	public static final String image_ext = ".jpg";
	
	public static final float scale = (float)1.5;
	public static final int period_multi = 2;
	public static final FloatPoint center = new FloatPoint(160, 200);
	public static final int timeout = 20000;
}
