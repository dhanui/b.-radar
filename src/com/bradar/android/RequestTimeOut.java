package com.bradar.android;

import java.util.TimerTask;
import org.apache.http.NameValuePair;
import android.os.AsyncTask;

public class RequestTimeOut extends TimerTask
{
	AsyncTask<NameValuePair, String, String> request;
	
	public RequestTimeOut(AsyncTask<NameValuePair, String, String> request)
	{
		this.request = request;
	}
	
	public void run()
	{
		request.cancel(true);
	}
}
