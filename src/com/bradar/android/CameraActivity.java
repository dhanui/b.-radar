package com.bradar.android;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CancellationException;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;
import com.bradar.android.R;
import android.media.ExifInterface;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.Camera;
import android.hardware.Camera.PictureCallback;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.FrameLayout;

public class CameraActivity extends Activity {

	private Context context = this;
	
	private Camera camera;
	private CameraPreview cameraPreview;
	private Button btnCapture;
	private WifiManager wifiManager;
	private List<ScanResult> results = new ArrayList<ScanResult>();
	
	private String chest_id;
	private String bssid;
	private String strength;
	private String imagePath;
	private float latitude;
	private float longitude;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_camera);

		// Fill parameters with the appropriate values
		chest_id = getIntent().getStringExtra("chest_id");
		bssid = getIntent().getStringExtra("bssid");
		strength = "" + 0;
		latitude = Float.parseFloat(getIntent().getStringExtra("latitude"));
		longitude = Float.parseFloat(getIntent().getStringExtra("longitude"));
		
		wifiManager = (WifiManager)getSystemService(WIFI_SERVICE);
		try
		{
			if (wifiManager.isWifiEnabled())
			{
				registerReceiver(new BroadcastReceiver() 
				{
					@Override
					public void onReceive(Context context, Intent intent) 
					{
						// TODO Auto-generated method stub
						results = wifiManager.getScanResults();
					}
				}, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
				
				wifiManager.startScan();
			}
		}
		catch (Exception e)
		{
			Utils.showDialogBox(this, "Error", e.getMessage(), null);
		}
		
		// Define camera preview screen
		cameraPreview = new CameraPreview(getApplicationContext());
		((FrameLayout)findViewById(R.id.preview)).addView(cameraPreview);
		
		// Define capture button
		btnCapture = (Button)findViewById(R.id.btnCapture);
		btnCapture.setOnClickListener(new OnClickListener() 
		{
			public void onClick(View v) 
			{
				cameraPreview.camera.takePicture(null, null, jpeg);
				cameraPreview.camera.startPreview();
			}
		});
	}
	
	PictureCallback jpeg = new PictureCallback() 
	{
		@SuppressLint("SimpleDateFormat")
		public void onPictureTaken(byte[] data, Camera camera) 
		{
			FileOutputStream fs = null;
			try
			{
				// Define unique filename
				String name = "IMG_" + new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
				if(!new File(Constants.image_dir).exists()) // Create directory if it doesn't exist
				{
					new File(Constants.image_dir).mkdirs();
				}
				imagePath = Constants.image_dir + name + Constants.image_ext;
				// Save image
				fs = new FileOutputStream(imagePath);
				fs.write(data);
				fs.close();
				geotag();
				Utils.showOptionBox(context, "Sending image confirmation", "Do you want to send this image", new DialogInterface.OnClickListener() 
				{
					public void onClick(DialogInterface dialog, int which) {
						acquireChest();
					}
				}, null);
			}
			catch(Exception e)
			{
				Utils.showDialogBox(context, "Error", e.getMessage(), null);
			}
		}
	};

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.camera, menu);
		return true;
	}
	
	public boolean onOptionsItemSelected(MenuItem item)
	{
		if (camera != null)
		{
			// If camera is active, release it
			camera.release();
			camera = null;
		}
		switch(item.getItemId())
		{
			case R.id.action_radar:
				// Switch to main activity
				Intent rIntent = new Intent(getApplicationContext(), MainActivity.class);
				startActivity(rIntent);
				break;
			default:
				break;
		}
		return super.onOptionsItemSelected(item);
	}
	
	private void geotag()
	{
		ExifInterface exif;
		
		try
		{
			exif = new ExifInterface(imagePath);
			// Determine location reference
			if (latitude >= 0)
			{
				exif.setAttribute(ExifInterface.TAG_GPS_LATITUDE_REF, "N");
			}
			else
			{
				exif.setAttribute(ExifInterface.TAG_GPS_LATITUDE_REF, "S");
				latitude = Math.abs(latitude);
			}
			if (longitude >= 0)
			{
				exif.setAttribute(ExifInterface.TAG_GPS_LONGITUDE_REF, "E");
			}
			else
			{
				exif.setAttribute(ExifInterface.TAG_GPS_LONGITUDE_REF, "W");
				longitude = Math.abs(longitude);
			}
			
			// Count location coordinate in degrees, minutes, and seconds
			int degreeLat = (int)Math.floor(latitude);
			int minuteLat = (int)Math.floor((latitude - degreeLat) * 60);
			float secondLat = (((latitude - degreeLat) * 60) - minuteLat) * 600000;
			
			int degreeLon = (int)Math.floor(longitude);
			int minuteLon = (int)Math.floor((longitude - degreeLon) * 60);
			float secondLon = (((longitude - degreeLon) * 60) - minuteLon) * 600000;
			
			exif.setAttribute(ExifInterface.TAG_GPS_LATITUDE, degreeLat + "/1," + minuteLat + "/1," + secondLat + "/10000");
			exif.setAttribute(ExifInterface.TAG_GPS_LONGITUDE, degreeLon + "/1," + minuteLon + "/1," + secondLon + "/10000");
			
			exif.saveAttributes();
		}
		catch (Exception e)
		{
			Utils.showDialogBox(this, "Error", e.getMessage(), null);
		}
	}
	
	private void acquireChest()
	{
		try
		{
			if (wifiManager.isWifiEnabled())
			{
				if (results.size() > 0)
				{	
					for (ScanResult result: results)
					{
						if (result.BSSID.equals(bssid))
						{
							// Set strength parameter to wifi signal level
							strength = "" + result.level;
							Utils.showDialogBox(this, "Wifi found", "Strength: " + strength, null);
							break;
						}
					}
				}
			}
			
			// HTTP POST request
			RequestPost request = new RequestPost();
			NameValuePair[] params = new NameValuePair[6];
			params[0] = new BasicNameValuePair("group_id", Constants.group_id);
			params[1] = new BasicNameValuePair("chest_id", chest_id);
			params[2] = new BasicNameValuePair("file", imagePath);
			params[3] = new BasicNameValuePair("bssid", bssid);
			params[4] = new BasicNameValuePair("wifi", strength);
			params[5] = new BasicNameValuePair("action", "acquire");
			request.execute(params);
			
			// Respose handler
			JSONObject response = new JSONObject(request.get());
			String status = response.getString("status");
			if (status.compareTo("success") == 0)
			{
				Utils.showDialogBox(this, "Success", "Acquire chest success", new DialogInterface.OnClickListener() 
				{
					public void onClick(DialogInterface dialog, int which) 
					{
						// Switch to main activity if acquire chest is successful
						Intent mIntent = new Intent(getApplicationContext(), MainActivity.class);
						startActivity(mIntent);
					}
				});
				
			}
			else if (status.compareTo("failed") == 0)
			{
				throw new Exception(response.getString("description"));
			}
		}
		catch (CancellationException e)
		{
			Utils.showDialogBox(this, "Error", "Request timeout", null);
		}
		catch (NullPointerException e)
		{
			Utils.showDialogBox(this, "Error", "Server error", null);
		}
		catch (Exception e)
		{
			Utils.showDialogBox(this,"Error", e.getMessage(), null);
		}
	}
}
