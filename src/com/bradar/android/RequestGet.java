package com.bradar.android;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Timer;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.os.AsyncTask;

public class RequestGet extends AsyncTask<NameValuePair, String, String>{
	
	@Override
	protected String doInBackground(NameValuePair...params)
	{
		HttpClient httpClient = new DefaultHttpClient();
		HttpResponse response;
		String responseString = null;
		
		// Define the complete URI for GET request
		String uri = Constants.web_uri + "?";
		for (int i = 0; i < params.length; i++)
		{
			uri += params[i].getName() + "=" + params[i].getValue();
			if (i < params.length - 1)
			{
				uri += "&";
			}
		}
		
		// Timeout timer
		Timer timer = new Timer();
		timer.schedule(new RequestTimeOut(this), Constants.timeout);
		
		try
		{
			response = httpClient.execute(new HttpGet(uri));
			StatusLine statusLine = response.getStatusLine();
			if (statusLine.getStatusCode() == HttpStatus.SC_OK)
			{
				timer.cancel();
				ByteArrayOutputStream out = new ByteArrayOutputStream();
				response.getEntity().writeTo(out);
				out.close();
				responseString = out.toString();
			}
			else
			{
				// Close the connection
				response.getEntity().getContent().close();
				throw new IOException(statusLine.getReasonPhrase());
			}
		}
		catch (Exception e)
		{
			timer.cancel();
			e.printStackTrace();
		}
		
		return responseString;
	}
}
