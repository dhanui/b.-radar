package com.bradar.android;

import java.util.List;
import android.content.Context;
import android.hardware.Camera;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class CameraPreview extends SurfaceView implements SurfaceHolder.Callback
{

	private SurfaceHolder holder;
	public Camera camera;
	private Context context;
	
	public CameraPreview(Context context)
	{
		super(context);
		
		this.context = context;
		holder = getHolder();
		holder.addCallback(this);
	}
	
	public void surfaceCreated(SurfaceHolder holder)
	{
		camera = Camera.open();
		camera.setDisplayOrientation(90);
		
		try
		{
			camera.setPreviewDisplay(holder);
		}
		catch (Exception e)
		{
			Utils.showDialogBox(context, "Error", e.getMessage(), null);
		}
	}
	
	public void surfaceDestroyed(SurfaceHolder holder)
	{
		camera.stopPreview();
		camera.release();
		camera = null;
	}
	
	public void surfaceChanged(SurfaceHolder holder, int format, int w, int h)
	{
		Camera.Parameters params = camera.getParameters();
		List<Camera.Size> previewSizes = params.getSupportedPreviewSizes();

	    Camera.Size previewSize = previewSizes.get(0);
		params.setPreviewSize(previewSize.width, previewSize.height);
		camera.setParameters(params);
		camera.startPreview();
	}
}
