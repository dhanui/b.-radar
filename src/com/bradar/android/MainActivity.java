package com.bradar.android;

import java.util.ArrayList;
import java.util.concurrent.CancellationException;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import com.bradar.android.R;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MainActivity extends Activity {

	private PowerManager powerManager;
	private WakeLock wakeLock;
	
	private ArrayList<Chest> chests = new ArrayList<Chest>();
	private GPS gps;
	private Compass compass;
	private RadarView radarView;
	
	private Button btnRadar;
	
	@SuppressLint("Wakelock")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		// Make sure screen and power stays on
		powerManager = (PowerManager)getSystemService(POWER_SERVICE);
		wakeLock = powerManager.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "Radar Wakelock");
		
		// Start sensor listener
		compass = new Compass(this);
		
		// Start GPS
		gps = new GPS(this);
				
		// Define button listener for Start Radar
		btnRadar = (Button)findViewById(R.id.btnRadar);
		btnRadar.setOnClickListener(new OnClickListener() 
		{
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startRadar();
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	private void startRadar()
	{
		// Retrieve unopened chests location
		retrieveChest();
		
		// Draw radar
		radarView = new RadarView(this, chests, gps, compass);
		setContentView(radarView);
		radarView.requestFocus();
	}
	
	@Override
	protected void onResume()
	{
		super.onResume();
		
		// Acquire wakelock
		wakeLock.acquire();
		
		// Start listener
		compass.startListener();
		gps.startListener();
	}
	
	@Override
	protected void onPause()
	{
		super.onPause();
		
		// Release wakelock
		wakeLock.release();
		
		// Stop listener
		compass.stopListener();
		gps.stopListener();
	}
	
	public boolean onOptionsItemSelected(MenuItem item)
	{
		switch(item.getItemId())
		{
			case R.id.action_camera:
				// Switch to camera activity if on tracking mode
				if (radarView.getTrackedChest() == null)
				{
					Utils.showDialogBox(this, "Error", "No chest is tracked", null);
				}
				else
				{
					// Switch to camera activity
					Intent cIntent = new Intent(getApplicationContext(), CameraActivity.class);
					cIntent.putExtra("chest_id", radarView.getTrackedChest().chest_id);
					cIntent.putExtra("bssid", radarView.getTrackedChest().bssid);
					cIntent.putExtra("latitude", "" + gps.getLatitude());
					cIntent.putExtra("longitude", "" + gps.getLongitude());
					startActivity(cIntent);
				}
				break;
			case R.id.action_refresh:
				// Refresh radar if on radar mode
				if (radarView.getTrackedChest() == null)
				{
					startRadar();
				}
				else
				{
					Utils.showDialogBox(this, "Error", "Radar is on tracking mode", null);
				}
				break;
			case R.id.action_count:
				countRemainingChest();
				break;
			case R.id.action_reset:
				// Reset chest if on radar mode
				if (radarView.getTrackedChest() == null)
				{
					Utils.showOptionBox(this, "Reset Confirmation", "Are you sure you want to reset chest status?", new DialogInterface.OnClickListener() 
					{
						public void onClick(DialogInterface dialog, int which) {
							resetChest();
						}
					}, null);
				}
				else
				{
					Utils.showDialogBox(this, "Error", "Radar is on tracking mode", null);
				}
				break;
			default:
				break;
		}
		return super.onOptionsItemSelected(item);
	}
	
	private void resetChest()
	{
		try
		{
			// HTTP POST request
			RequestPost request = new RequestPost();
			NameValuePair[] params = new NameValuePair[2];
			params[0] = new BasicNameValuePair("group_id", Constants.group_id);
			params[1] = new BasicNameValuePair("action", "reset");
			request.execute(params);
			
			// Response handler
			JSONObject response = new JSONObject(request.get());
			String status = response.getString("status");
			if (status.compareTo("success") == 0)
			{
				Utils.showDialogBox(this, "Success", "Reset chest status success", null);
			}
			else if (status.compareTo("failed") == 0)
			{
				throw new Exception(response.getString("description"));
			}
		}
		catch (CancellationException e)
		{
			Utils.showDialogBox(this, "Error", "Request timeout", null);
		}
		catch (NullPointerException e)
		{
			Utils.showDialogBox(this, "Error", "Server error", null);
		}
		catch (Exception e)
		{
			Utils.showDialogBox(this,"Error", e.getMessage(), null);
		}
	}
	
	private void retrieveChest()
	{
		// Clear chest list
		chests.clear();
		
		try
		{
			// HTTP GET request
			RequestGet request = new RequestGet();
			NameValuePair[] params = new NameValuePair[4];
			params[0] = new BasicNameValuePair("group_id", Constants.group_id);
			params[1] = new BasicNameValuePair("action", "retrieve");
			params[2] = new BasicNameValuePair("latitude", "" + gps.getLatitude());
			params[3] = new BasicNameValuePair("longitude", ""  + gps.getLongitude());
			request.execute(params);
			
			// Response handler
			JSONObject response = new JSONObject(request.get());
			String status = response.getString("status");
			if (status.compareTo("success") == 0)
			{
				if (response.getString("data").compareTo("null") != 0)
				{
					JSONArray datas = response.getJSONArray("data");
					if (datas.length() > 0)
					{
						for (int i = 0; i < datas.length(); i++)
						{
							JSONObject data = datas.getJSONObject(i);
							String id = data.getString("id");
							String bssid = data.getString("bssid");
							double distance = data.getDouble("distance");
							int degree = data.getInt("degree");
							chests.add(new Chest(id, distance, degree, bssid));
						}
					}
				}
				else
				{
					if (gps.getLatitude() == 0 && gps.getLongitude() == 0)
					{
						throw new Exception("GPS cannot find current location");
					}
					else
					{
						throw new  Exception("No chests nearby");
					}
				}
				if (chests.size() > 0)
				{
					Utils.showDialogBox(this, "Success", chests.size() + " chest(s) found", null);
				}
				else
				{
					Utils.showDialogBox(this, "Error", "No chests found", null);
				}
			}
			else if (status.compareTo("failed") == 0)
			{
				throw new Exception(response.getString("description"));
			}
		}
		catch (CancellationException e)
		{
			Utils.showDialogBox(this, "Error", "Request timeout", null);
		}
		catch (NullPointerException e)
		{
			Utils.showDialogBox(this, "Error", "Server error", null);
		}
		catch (Exception e)
		{
			Utils.showDialogBox(this, "Error", e.getMessage(), null);
		}
	}
	

	
	private void countRemainingChest()
	{
		try
		{
			// HTTP GET request
			RequestGet request = new RequestGet();
			NameValuePair[] params = new NameValuePair[2];
			params[0] = new BasicNameValuePair("group_id", Constants.group_id);
			params[1] = new BasicNameValuePair("action", "number");
			request.execute(params);
			
			// Response handler
			JSONObject response = new JSONObject(request.get());
			String status = response.getString("status");
			if (status.compareTo("success") == 0)
			{
				Utils.showDialogBox(this, "Chest Info", "Chest(s) remaing: " + response.getInt("data"), null);
			}
			else
			{
				throw new Exception(response.getString("description"));
			}
		}
		catch (CancellationException e)
		{
			Utils.showDialogBox(this, "Error", "Request timeout", null);
		}
		catch (NullPointerException e)
		{
			Utils.showDialogBox(this, "Error", "Server error", null);
		}
		catch (Exception e)
		{
			Utils.showDialogBox(this, "Error", e.getMessage(), null);
		}
	}
}
