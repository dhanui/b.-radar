package com.bradar.android;

public class FloatPoint {
	public float x, y;
	
	public FloatPoint()
	{
		this(0, 0);
	}
	
	public FloatPoint(float x, float y)
	{
		this.x = x;
		this.y = y;
	}
	
	public FloatPoint(FloatPoint point)
	{
		x = point.x;
		y = point.y;
	}
}
