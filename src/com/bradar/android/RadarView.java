package com.bradar.android;

import java.util.ArrayList;
import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.media.MediaPlayer;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

public class RadarView extends View implements OnTouchListener
{
	public static final int RADAR_MODE = 0;
	public static final int TRACKING_MODE = 1;
	private static final int threshold = 10;
	
	private Context context;
	private int mode;
	private int period;
	private int cooldown;
	
	private ArrayList<Chest> chests = new ArrayList<Chest>();
	private Paint yellow = new Paint();
	private Paint white = new Paint();
	private Paint green = new Paint();
	private Chest trackChest;
	private GPS gps;
	private Compass compass;
	private Bitmap arrowUp;
	private Bitmap resizedBitmap;
	private MediaPlayer beep;
	
	private float latitude = 0;
	private float longitude = 0;
	
	private float chestLatitude;
	private float chestLongitude;
	
	public RadarView(Context context)
	{
		super(context);
		this.context = context;
		mode = RADAR_MODE;
		
		setFocusable(true);
		setFocusableInTouchMode(true);
		
		this.setOnTouchListener(this);
		
		// Define colors
		yellow.setColor(Color.YELLOW);
		yellow.setAntiAlias(true);
		white.setColor(Color.WHITE);
		white.setStyle(Paint.Style.STROKE);
		green.setColor(Color.GREEN);
		green.setAntiAlias(true);
		
		// Define beep sound
		beep = MediaPlayer.create(context,R.raw.beeps);
		
		// Define arrow image
		arrowUp = BitmapFactory.decodeResource(getResources(), R.drawable.arrow_up);
	}
	
	public RadarView(Context context, ArrayList<Chest> chests, GPS gps, Compass compass)
	{
		this(context);
		this.chests = chests;
		this.compass = compass;
		this.gps = gps;
	}
	
	@SuppressLint("DrawAllocation")
	@Override
	public void onDraw(Canvas canvas)
	{
		if (mode == RADAR_MODE)
		{
			// Draw radar background
			canvas.drawColor(Color.BLACK);
			canvas.drawCircle(Constants.center.x, Constants.center.y, 190, green);
			canvas.drawCircle(Constants.center.x, Constants.center.y, 30, white);
			canvas.drawCircle(Constants.center.x, Constants.center.y, 80, white);
			canvas.drawCircle(Constants.center.x, Constants.center.y, 130, white);
			
			// Draw chests on its location
			if (chests.size() > 0)
			{
				for (Chest chest : chests)
				{
					canvas.drawCircle(chest.getPosition().x, chest.getPosition().y, 5, yellow);
				}
			}
			invalidate();
		}
		else if (mode == TRACKING_MODE)
		{
			if (latitude != gps.getLatitude() || longitude != gps.getLongitude())
			{
				// Update chest information if location has changed
				latitude = gps.getLatitude();
				longitude = gps.getLongitude();
				updateChestDistance();
				updateChestDegree();
			}
			
			// Draw background
			canvas.drawColor(Color.GREEN);
			
			// Rotate canvas according to chest location
			canvas.rotate(trackChest.getRelativeDegree(compass.getDegree()), Constants.center.x, Constants.center.y);
			
			// Define image size and sound period according to chest distance
			if(trackChest.distance >= 100)
			{
				resizedBitmap = Bitmap.createScaledBitmap(arrowUp, 3, 3, false);
				period = 1000;
			}
			else
			{
				resizedBitmap = Bitmap.createScaledBitmap(arrowUp, (300 - (3 * (int)trackChest.distance)), (300 - (3 * (int)trackChest.distance)), false);
				period = (int)trackChest.distance * Constants.period_multi;
			}
			
			// Draw arrow image
			canvas.drawBitmap(resizedBitmap, Constants.center.x - (resizedBitmap.getWidth() / 2), Constants.center.y - (resizedBitmap.getHeight() / 2), yellow);
			
			if (cooldown > 0)
			{
				cooldown--;
			}
			else
			{
				beep.start();
				cooldown = period;
			}
			
			// Refresh canvas
			invalidate();
		}
	}
	
	public boolean onTouch(View view, MotionEvent event)
	{
		if (mode == RADAR_MODE)
		{
			FloatPoint point = new FloatPoint(event.getX(), event.getY());
			trackChest = getChest(point);
			if (trackChest != null)
			{
				// Start tracking selected chest
				mode = TRACKING_MODE;
				setChestLatLon();
				Utils.showDialogBox(context, "Start tracking", "ID: " + trackChest.bssid + "\nDistance: " + trackChest.distance, null);
				invalidate();
			}
		}
		return true;
	}
	
	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
		if (keyCode == KeyEvent.KEYCODE_BACK && mode == TRACKING_MODE)
		{
			// Return to radar mode
			mode = RADAR_MODE;
			trackChest = null;
			latitude = 0;
			longitude = 0;
			invalidate();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
	
	private Chest getChest(FloatPoint point)
	{
		for (Chest chest : chests)
		{
			if (checkCoord(chest, point))
			{
				return chest;
			}
		}
		return null;
	}
	
	private boolean checkCoord(Chest chest, FloatPoint point)
	{
		float divX = Math.abs(chest.getPosition().x - point.x);
		float divY = Math.abs(chest.getPosition().y - point.y);
		
		return ((divX < threshold) && (divY < threshold));
	}
	
	private void setChestLatLon()
	{
		double deltaX = trackChest.distance * Math.sin(Math.toRadians(trackChest.degree));
		double deltaY = trackChest.distance * Math.cos(Math.toRadians(trackChest.degree));
		
		double deltaLon = deltaX / (111320 * Math.cos(Math.toRadians(gps.getLatitude())));
		double deltaLat = deltaY / 110540;
		
		chestLatitude = gps.getLatitude() + (float)deltaLat;
		chestLongitude = gps.getLongitude() + (float)deltaLon;
	}
	
	private void updateChestDistance()
	{
		double radLat1 = Math.toRadians(latitude);
		double radLon1 = Math.toRadians(longitude);
		double radLat2 = Math.toRadians(chestLatitude);
		double radLon2 = Math.toRadians(chestLongitude);
		
		double radDist = Math.acos(Math.sin(radLat1) * Math.sin(radLat2) + Math.cos(radLat1) * Math.cos(radLat2) * Math.cos(radLon2 - radLon1)) * 6371000;
		trackChest.distance = (float)Math.toDegrees(radDist) / 100;
	}
	
	private void updateChestDegree()
	{
		double radLat1 = Math.toRadians(latitude);
		double radLon1 = Math.toRadians(longitude);
		double radLat2 = Math.toRadians(chestLatitude);
		double radLon2 = Math.toRadians(chestLongitude);
		
		double y = Math.sin(radLon2 - radLon1) * Math.cos(radLat2);
		double x = Math.cos(radLat1) * Math.sin(radLat2) - Math.sin(radLat1) * Math.cos(radLat2) * Math.cos(radLon2 - radLon1);
		double radBearing = Math.atan2(y, x);
		trackChest.degree = (int)Math.toDegrees(radBearing);
	}
	
	public Chest getTrackedChest()
	{
		return trackChest;
	}
}