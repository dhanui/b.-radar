package com.bradar.android;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Timer;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import android.os.AsyncTask;

@SuppressWarnings("deprecation")
public class RequestPost extends AsyncTask<NameValuePair, String, String>{
	
	@Override
	protected String doInBackground(NameValuePair...params)
	{
		HttpClient httpClient = new DefaultHttpClient();
		HttpPost httpPost = new HttpPost(Constants.web_uri);
		MultipartEntity entity = new MultipartEntity();
		HttpResponse response;
		String responseString = null;
		
		// Timeout timer
		Timer timer = new Timer();
		timer.schedule(new RequestTimeOut(this), Constants.timeout);
		
		try
		{
			// Define the parameters for post request
			for (int i = 0; i < params.length; i++)
			{
				if (params[i].getName().equals("file")) // File upload
				{
					entity.addPart(params[i].getName(), new FileBody(new File(params[i].getValue())));
				}
				else // String parameter
				{
					entity.addPart(params[i].getName(), new StringBody(params[i].getValue()));
				}
			}
			
			httpPost.setEntity(entity);
			response = httpClient.execute(httpPost);
			StatusLine statusLine = response.getStatusLine();
			if (statusLine.getStatusCode() == HttpStatus.SC_OK)
			{
				timer.cancel();
				ByteArrayOutputStream out = new ByteArrayOutputStream();
				response.getEntity().writeTo(out);
				out.close();
				responseString = out.toString();
			}
			else
			{
				// Close the connection
				response.getEntity().getContent().close();
				throw new IOException(statusLine.getReasonPhrase());
			}
		}
		catch (Exception e)
		{
			timer.cancel();
			e.printStackTrace();
		}
		
		return responseString;
	}
}
