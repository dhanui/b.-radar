package com.bradar.android;

public class Chest {
	
	public String chest_id;
	public float distance;
	public int degree;
	public String bssid;
	
	private FloatPoint point;
	
	public Chest(String chest_id, double distance, int degree, String bssid)
	{
		this.chest_id = chest_id;
		this.distance = (float)distance;
		this.degree = degree;
		this.bssid = bssid;
		point = new FloatPoint();
		setAbsolutePoint();
	}
	
	private void setAbsolutePoint()
	{
		point.x = (float)(Math.cos((Math.toRadians(degree) - 90)) * distance * Constants.scale) + Constants.center.x;
		point.y = (float)(Math.sin((Math.toRadians(degree) - 90)) * distance * Constants.scale) + Constants.center.y;
	}
	
	public int getRelativeDegree(int phoneDegree)
	{
		if (degree < phoneDegree)
		{
			degree += 360;
		}
		return (degree - phoneDegree);
	}
	
	public FloatPoint getPosition()
	{
		return point;
	}
}
