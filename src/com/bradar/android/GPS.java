package com.bradar.android;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;

public class GPS extends Service implements LocationListener {
	
	private Location location = null;
	private double latitude = 0;
	private double longitude = 0;
	
	protected LocationManager locationManager;
	
	public GPS(Context context)
	{
		locationManager = (LocationManager)context.getSystemService(LOCATION_SERVICE);
	}
	
	@SuppressLint("NewApi")
	public Location startListener()
	{
		try
		{
			if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER))
			{
				// Get Location using GPS
				locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 1, this);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		return location;
	}
	
	public void stopListener()
	{
		if (locationManager != null)
		{
			locationManager.removeUpdates(this);
		}
	}
	
	@Override
    public void onLocationChanged(Location location) {
		this.location = location;
		latitude = this.location.getLatitude();
		longitude = this.location.getLongitude();
    }
 
    @Override
    public void onProviderDisabled(String provider) {
    }
 
    @Override
    public void onProviderEnabled(String provider) {
    }
 
    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }
 
    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }
	
	public float getLatitude()
	{
		return (float)latitude;
	}
	
	public float getLongitude()
	{
		return (float)longitude;
	}
}
