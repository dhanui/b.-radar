package com.bradar.android;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

public class Utils {

	public static void showDialogBox(Context context, String title, String message, DialogInterface.OnClickListener ok)
	{
		AlertDialog.Builder dlgAlert = new AlertDialog.Builder(context);
		dlgAlert.setMessage(message);
		dlgAlert.setTitle(title);
		dlgAlert.setPositiveButton("OK", ok);
		dlgAlert.setCancelable(true);
		dlgAlert.create().show();
	}
	
	public static void showOptionBox(Context context, String title, String message, DialogInterface.OnClickListener yes, DialogInterface.OnClickListener no)
	{
		AlertDialog.Builder dlgOption = new AlertDialog.Builder(context);
		dlgOption.setMessage(message);
		dlgOption.setTitle(title);
		dlgOption.setPositiveButton("Yes", yes);
		dlgOption.setNegativeButton("No", no);
		dlgOption.setCancelable(true);
		dlgOption.create().show();
	}
}
